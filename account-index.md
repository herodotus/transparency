# 软件默认账号列表

| 软件名称 | 默认账号 | 默认密码 |
| -------- | -------- | -------- |
| grafana  | admin    | admin    |
| nacos    | nacos    | nacos    |
