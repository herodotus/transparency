# 按软件端口索引的默认端口列表

| 端口  | 软件名称                     | 用途说明（可选）                                                         |
| :---: | ---------------------------- | ------------------------------------------------------------------------ |
|  80   | nginx                        | Nginx 默认访问端口                                                       |
| 1433  | sql_server                   | Sql Server 数据库默认访问端口                                            |
| 1521  | oracle                       | Oracle 数据库默认访问端口                                                |
| 1883  | emqx                         | Emqx 用于 MQTT over TCP 监听器                                           |
| 2181  | zookeeper                    | Zookeeper 服务端供客户端连接的端口                                       |
| 2888  | zookeeper                    | Zookeeper 集群之间进行数据同步的端口                                     |
| 3000  | grafana                      | Grafana 默认访问端口                                                     |
| 3306  | mysql                        | MySQL 数据库默认访问端口                                                 |
| 3888  | zookeeper                    | Zookeeper 集群之间进行 Leader 选 \* 举的端口                             |
| 5000  | db2                          | DB2 数据库默认访问端口                                                   |
| 5432  | postgresql                   | PostgreSQL 数据库默认访问端口                                            |
| 6379  | redis                        | Redis 内存数据库默认访问端口                                             |
| 7848  | nacos                        | Nacos Jraft 请求服务端端口，用于处理服务端间的 Raft 相关请求             |
| 8080  | 1. tomcat<br/>2. polarismesh | 1. Tomcat 默认端口<br/>2. Polaris 可视化管理控制台访问端口               |
| 8083  | emqx                         | Emqx 用于 MQTT over WebSocket 监听器                                     |
| 8084  | emqx                         | Emqx 用于 MQTT over WSS (WebSocket over SSL) 监听器                      |
| 8086  | influxdb                     | InfluxDB 时序数据库默认访问端                                            |
| 8090  | polarismesh                  | Polaris 注册中心端口 K8S 环境下必需端口                                  |
| 8091  | polarismesh                  | Polaris 注册中心端口 GRPC 端口                                           |
| 8093  | polarismesh                  | Polaris 配置中心端口 GRPC 端口                                           |
| 8100  | polarismesh                  | Polaris 流量控制端口 GRPC 端口                                           |
| 8101  | polarismesh                  | Polaris 流量控制端口 GRPC 端口                                           |
| 8500  | consul                       | Consul 默认访问端口                                                      |
| 8761  | polarismesh                  | Polaris 兼容 Eureka 端口                                                 |
| 8848  | nacos                        | Nacos 控制台访问端口                                                     |
| 8883  | emqx                         | Emqx 用于 MQTT over SSL/TLS 监听器                                       |
| 9090  | prometheus                   | Prometheus 开源监控告警系统默认访问端口                                  |
| 9091  | pushgateway                  | Prometheus 生态组件，用于被动推送监控数据的默认口                        |
| 9092  | kafka                        | Kafka 消息队列默认端口                                                   |
| 9100  | node_exporter                | Prometheus 生态组件，用于采集 Linux 内核服务器层面的运行指标的默认端口   |
| 9182  | windows_exporter             | Prometheus 社区组件，用于采集 Windows 内核服务器层面的运行指标的默认端口 |
| 9848  | nacos                        | Nacos 客户端 gRPC 请求服务端端口，用于客户端向服务端发起连接和请求       |
| 9849  | nacos                        | Nacos 服务端 gRPC 请求服务端端口，用于服务间同步等                       |
| 15010 | polarismesh                  | 用途待补充                                                               |
| 18083 | emqx                         | Emqx HTTP API 服务的默认监听端口，Dashboard 功能也依赖于这个端           |
| 27017 | mongodb                      | MongoDB 数据库默认访问端口                                               |
